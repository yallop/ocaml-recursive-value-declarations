A work-in-progress article on recursive value declarations in OCaml
(Alban Reynaud, Gabriel Scherer, Jeremy Yallop)

See also Alban's internship report at
  https://github.com/Cemoixerestre/Internship-2018-ocaml-recursive-value
